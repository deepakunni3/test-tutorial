.. test-tutorial documentation master file, created by
   sphinx-quickstart on Mon Jul  6 21:00:48 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to test-tutorial's documentation!
=========================================

Another simple header
=====================
Here is some thext explaining some very complicated stuff.::

    print 'hello'
    >> hello

Guide:
^^^^^^^^^

.. toctree::
    :maxdepth: 2

    license
    help


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

